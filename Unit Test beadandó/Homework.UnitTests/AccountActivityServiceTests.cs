﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class AccountActivityServiceTests
    {
        private IAccountRepository _accountRepository;
        private IAccountActivityService _accountActivityService;
        
        [SetUp]
        public void SetUp()
        {
            _accountRepository = new FakeAccountRepository();
            _accountActivityService = new AccountActivityService(_accountRepository);
        }

        [Test]
        public void GivenNotExistingAccountId_GetActivity_ThrowsAccountNotExistsException()
        {
            Assert.That(() => { _accountActivityService.GetActivity(0); }, Throws.InstanceOf<AccountNotExistsException>());
        }

        [Test]
        public void GivenExistingAccountIdWithZeroActionPerformed_GetActivity_ActivityLevelIsNone()
        {
            Account account = new Account(0);
            _accountRepository.Add(account);
            Assert.That(_accountActivityService.GetActivity(account.Id), Is.EqualTo(ActivityLevel.None));
        }

        [Test]
        public void GivenValidAccountIdWithLessThanTwentyActionsPerformed_GetActivity_ActivityLevelIsLow([Range(1, 19)] int x)
        {
            Account account = new Account(0);
            account.Activate();

            for (int i = 0; i < x; i++)
            {
                account.TakeAction(new FakeAction(true));
            }

            _accountRepository.Add(account);

            Assert.That(_accountActivityService.GetActivity(account.Id), Is.EqualTo(ActivityLevel.Low));
        }

        [Test]
        public void GivenValidAccountIdWithLessThanFortyActionsPerformed_GetActivity_ActivityLevelIsMedium([Range(20, 39)] int x)
        {
            Account account = new Account(0);
            account.Activate();

            for (int i = 0; i < x; i++)
            {
                account.TakeAction(new FakeAction(true));
            }

            _accountRepository.Add(account);

            Assert.That(_accountActivityService.GetActivity(account.Id), Is.EqualTo(ActivityLevel.Medium));
        }

        [TestCase(40)]
        [TestCase(41)]
        [TestCase(400)]
        public void GivenValidAccountIdWithMoreThanOrEqualsWithFortyActionsPerformed_GetActivity_ActivityLevelIsMedium(int x)
        {
            Account account = new Account(0);
            account.Activate();

            for (int i = 0; i < x; i++)
            {
                account.TakeAction(new FakeAction(true));
            }

            _accountRepository.Add(account);

            Assert.That(_accountActivityService.GetActivity(account.Id), Is.EqualTo(ActivityLevel.High));
        }

        [Test]
        public void GivenZeroAccount_GetAmountForActivity_ReturnsZero()
        {
            int result = 0;
            foreach (ActivityLevel level in Enum.GetValues(typeof(ActivityLevel)))
            {
                result += _accountActivityService.GetAmountForActivity(level);
            }

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void GivenListOfAccounts_GetAmountForActivity_ReturnsCorrectValue()
        {
            int nextId = 0;
            List<Account> accounts = new List<Account>();

            Enum.GetValues<ActivityLevel>().ToList().ForEach(activityLevel =>
            {
                accounts.AddRange(Enumerable.Range(0, 5).Select(x => SetAccountActivity(new Account(nextId++), activityLevel)));
                accounts.ForEach(x => _accountRepository.Add(x));
                int result = _accountActivityService.GetAmountForActivity(activityLevel);
                Assert.That(result, Is.EqualTo(5));
            });
        }

        private Account SetAccountActivity(Account account, ActivityLevel level)
        {
            int activity = 0;
            switch (level)
            {
                default:
                case ActivityLevel.None:
                    break;
                case ActivityLevel.Low:
                    activity = 1;
                    break;
                case ActivityLevel.Medium:
                    activity = 22;
                    break;
                case ActivityLevel.High:
                    activity = 44;
                    break;
            }
            
            account.Activate();

            for (int i = 0; i < activity; i++)
            {
                account.TakeAction(new FakeAction(true));
            }

            return account;
        }
    }
}
