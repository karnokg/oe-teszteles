﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class FakeAction : IAction
    {
        public bool Result { get; set; }

        public FakeAction(bool result)
        {
            Result = result;
        }

        public bool Execute()
        {
            return Result;
        }
    }
}
