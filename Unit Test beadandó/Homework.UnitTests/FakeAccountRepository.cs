﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class FakeAccountRepository : IAccountRepository
    {
        private List<Account> _accounts = new List<Account>();

        public bool Add(Account account)
        {
            if (Exists(account.Id))
            {
                return false;
            }

            _accounts.Add(account);
            return true;
        }

        public bool Exists(int accountId)
        {
            return _accounts.Where(x => x.Id == accountId).Count() == 1;
        }

        public Account Get(int accountId)
        {
            return _accounts.Where(x => x.Id == accountId).FirstOrDefault();
        }

        public IEnumerable<Account> GetAll()
        {
            return _accounts.AsReadOnly();
        }

        public bool Remove(int accountId)
        {
            if (!Exists(accountId))
            {
                return false;
            }

            _accounts.RemoveAll(x => x.Id == accountId);
            return true;
        }
    }
}
