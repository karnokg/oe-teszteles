﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
namespace Homework.UnitTests
{
    public class AccountTests
    {
        private Mock<IAction> _mockAction;
        private Account _account;
        
        [SetUp]
        public void SetUp()
        {
            _mockAction = new Mock<IAction>();
            _account = new Account(1);
        }

        [Test]
        public void GivenRegistering_Register_Success()
        {
            // ARRANGE //ACT
            _account.Register();

            // ASSERT
            Assert.That(_account.IsRegistered, Is.True);
        }

        [Test]
        public void GivenActivating_Activate_Success()
        {
            // ARRANGE //ACT
            _account.Activate();

            // ASSERT
            Assert.That(_account.IsConfirmed, Is.True);
        }

        [Test]
        [Category("TakeAction")]
        public void GivenInvalidAccountNotRegisteredAndNotActivated_TakeAction_ThrowsInactiveUserException()
        {
            // ARRANGE
            _mockAction.Setup(x => x.Execute()).Returns(true);
            //IAction action = new FakeAction(true);

            // ACT ASSERT
            Assert.That(() => { _account.TakeAction(_mockAction.Object); }, Throws.InstanceOf<InactiveUserException>());
        }

        [Test]
        [Category("TakeAction")]
        public void GivenValidAccount_TakeAction_PerformsActionWithSuccess()
        {
            _account.Register();
            //IAction action = new FakeAction(true);
            _mockAction.Setup(x => x.Execute()).Returns(true);

            int before = _account.ActionsSuccessfullyPerformed;
            bool result = _account.TakeAction(_mockAction.Object);

            _mockAction.Verify(x => x.Execute(), Times.Once); 
            Assert.That(result, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(before + 1));
        }

        [Test]
        [Category("TakeAction")]
        public void GivenValidAccount_TakeAction_PerformsActionWithFail()
        {
            _account.Register();
            //IAction action = new FakeAction(false);
            _mockAction.Setup(x => x.Execute()).Returns(false);

            int before = _account.ActionsSuccessfullyPerformed;
            bool result = _account.TakeAction(_mockAction.Object);

            _mockAction.Verify(x => x.Execute(), Times.Once); 
            Assert.That(result, Is.False);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(before));
        }

        [Test]
        [Category("TakeAction")]
        public void GivenAccountNotActivated_TakeAction_PerformsAction()
        {
            // ARRANGE
            _account.Register();
            //IAction action = new FakeAction(true);
            _mockAction.Setup(x => x.Execute()).Returns(true);

            // ACT
            bool result = _account.TakeAction(_mockAction.Object);

            // ASSERT
            Assert.That(result, Is.True);
        }

        [Test]
        [Category("TakeAction")]
        public void GivenAccountNotRegistered_TakeAction_PerformsAction()
        {
            // ARRANGE
            _account.Activate();
            //IAction action = new FakeAction(true);
            _mockAction.Setup(x => x.Execute()).Returns(true);

            // ACT
            bool result = _account.TakeAction(_mockAction.Object);

            // ASSERT
            Assert.That(result, Is.True);
        }
    }
}
