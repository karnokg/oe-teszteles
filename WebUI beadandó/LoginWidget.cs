using OpenQA.Selenium;

namespace beadando
{
    internal class LoginWidget : BasePage
    {

        public LoginWidget(IWebDriver driver) : base(driver) { }
        public IWebElement InputEmailAddress => Driver.FindElement(By.CssSelector("input[type='email']"));
        public IWebElement InputPassword => Driver.FindElement(By.CssSelector("input[type='password']"));

        //public IWebElement ButtonSignIn => Driver.FindElement(By.CssSelector("button.btn-lg"));
        public IWebElement ButtonSignIn => Driver.FindElement(By.Id("signInSubmit"));

        public void SetEmail(string email)
        {
            InputEmailAddress.SendKeys(email);
        }

        public void SetPassword(string password)
        {
            InputPassword.SendKeys(password);
        }

        public LoginPage ClickLoginButton()
        {
            ButtonSignIn.Click();
            return new LoginPage(Driver);
        }

        public LoginPage Login(LoginModel login)
        {
            SetEmail(login.Email);
            SetPassword(login.Password);
            return ClickLoginButton();
        }
    }
}