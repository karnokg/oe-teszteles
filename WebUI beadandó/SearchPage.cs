using OpenQA.Selenium;

namespace beadando
{
    class SearchPage : MainPage
    {

        public SearchPage(IWebDriver driver) : base(driver) { }
        
        public SearchResultWidget GetSearchResultWidget()
        {
            return new SearchResultWidget(Driver);
        }
    }
}