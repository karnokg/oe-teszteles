using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    class SearchResultWidget : BasePage
    {

        public SearchResultWidget(IWebDriver driver) : base(driver) { }

        public IWebElement TextResult => Driver.FindElement(By.CssSelector("h1[class='findHeader']"));

        public string GetResultText()
        {
            Console.WriteLine(TextResult.Text);
            return TextResult.Text;
        }

    }
}
