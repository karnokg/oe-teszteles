using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    class MainPage : BasePage
    {
        public MainPage(IWebDriver webDriver) : base(webDriver) { }

        public static MainPage Navigate(IWebDriver webDriver)
        {
            
            webDriver.Navigate().GoToUrl("https://imdb.com");
            return new MainPage(webDriver);
        }

        public MainPageSearchWidget GetMainPageSearchWidget()
        {
            return new MainPageSearchWidget(Driver);
        }
    }
}
