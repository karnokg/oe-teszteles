using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace beadando
{
    class LoginResultWidget : BasePage
    {
        public LoginResultWidget(IWebDriver driver) : base(driver) { }

       // private IWebElement alert => Driver.FindElement(By.CssSelector("div .alert-danger"));
        private IList<IWebElement> alerts => Driver.FindElements(By.CssSelector("div[class='a-alert-content']"));

        public bool AnyErrors()
        {
            return alerts.Any(x => x.Displayed);
        }
    }
}