using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    class EpisodesPage : BasePage
    {
        public EpisodesPage(IWebDriver webDriver) : base(webDriver) { }

        public static EpisodesPage Navigate(IWebDriver webDriver, string id)
        {
            webDriver.Navigate().GoToUrl("https://imdb.com/title/" + id + "/episodes");
            return new EpisodesPage(webDriver);
        }

        public EpisodesWidget GetEpisodesWidget()
        {
            return new EpisodesWidget(Driver);
        }
    }
}
