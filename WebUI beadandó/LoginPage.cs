using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    class LoginPage : BasePage
    {
        public LoginPage(IWebDriver webDriver) : base(webDriver) { }

        public static LoginPage Navigate(IWebDriver webDriver)
        {
            //            webDriver.Navigate().GoToUrl("https://mydramalist.com/signin");
            webDriver.Navigate().GoToUrl("https://www.imdb.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.imdb.com%2Fregistration%2Fap-signin-handler%2Fimdb_us&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=imdb_us&openid.mode=checkid_setup&siteState=eyJvcGVuaWQuYXNzb2NfaGFuZGxlIjoiaW1kYl91cyIsInJlZGlyZWN0VG8iOiJodHRwczovL3d3dy5pbWRiLmNvbS8_cmVmXz1sb2dpbiJ9&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&tag=imdbtag_reg-20");
            return new LoginPage(webDriver);
        }
    
        public LoginWidget GetLoginWidget()
        {
            return new LoginWidget(Driver);
        }

        public LoginDetailsWidget GetLoginDetailsWidget()
        {
            return new LoginDetailsWidget(Driver);
        }

        public LoginResultWidget GetLoginResultWidget()
        {
            return new LoginResultWidget(Driver);
        }
    }
}
