using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace beadando
{
    class LoginDetailsWidget : BasePage
    {
        public LoginDetailsWidget(IWebDriver driver) : base(driver) { }

        private IWebElement linkDetails => Driver.FindElement(By.Id("remember_me_learn_more_link"));
        private IWebElement popOver => Driver.FindElement(By.Id("a-popover-1"));

        public LoginDetailsWidget ClickOnDetails()
        {
            linkDetails.Click();
            return new LoginDetailsWidget(Driver);
        }

        public bool IsVisible()
        {
            return popOver.GetCssValue("visibility") == "visible";
        }
    }
}