using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;

namespace beadando
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test]
        public void GivenEmptyFields_LoginPage_ShowsAlert()
        {
            LoginModel loginModel = new LoginModel { Email = "", Password = "" };

            var isAlertVisible = LoginPage.Navigate(Driver).GetLoginWidget().Login(loginModel).GetLoginResultWidget().AnyErrors();

            Assert.That(isAlertVisible, Is.True);
        }

        [Test, TestCaseSource("TestData")]
        public void GivenSearch_Page_ReturnsCorrectLabel(string search)
        {
            var result = MainPage.Navigate(Driver).GetMainPageSearchWidget().SearchFor(search).GetSearchResultWidget().GetResultText();
            var valid = result.EndsWith("\"" + search + "\"");
            Assert.That(valid, Is.True);
        }

        [Test]
        public void GivenClickOnDetails_LoginPage_ShowsPopUp()
        {
            bool visible = LoginPage.Navigate(Driver).GetLoginDetailsWidget().ClickOnDetails().IsVisible();
            Assert.That(visible, Is.True);
        }

        static IEnumerable TestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return from vars in doc.Descendants("testData")
                   let search = vars.Attribute("search").Value
                   select new object[] { search };
        }

        static IEnumerable EpisodesTestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\episodesdata.xml");
            return from vars in doc.Descendants("testData")
                   let id = vars.Attribute("id").Value
                   let year = vars.Attribute("year").Value
                   select new object[] { id, year };
        }
    }
}
