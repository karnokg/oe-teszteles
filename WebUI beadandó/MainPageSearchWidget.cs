using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    class MainPageSearchWidget : BasePage
    {

        public MainPageSearchWidget(IWebDriver driver) : base(driver) { }

        private IWebElement InputSearchBox => Driver.FindElement(By.CssSelector("input[type='text']"));
        private IWebElement ButtonSearch => Driver.FindElement(By.Id("suggestion-search-button"));
        
        public void SetSearch(string text)
        {
            InputSearchBox.SendKeys(text);
        }

        public SearchPage SearchFor(string text)
        {
            SetSearch(text);
            ButtonSearch.Click();
            return new SearchPage(Driver);
        }
    }
}
