using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace beadando
{
    class EpisodesWidget : BasePage
    {
        public EpisodesWidget(IWebDriver driver) : base(driver) { }

        private IWebElement year => Driver.FindElement(By.Id("byYear"));
        private IList<IWebElement> airdates => Driver.FindElements(By.CssSelector("airdate"));

        public void SetYear(int i)
        {
            new SelectElement(year).SelectByValue(i.ToString());
        }

        public HashSet<int> GetAirYears(int i)
        {
            SetYear(i);

            Thread.Sleep(2000);
            return airdates.ToList().Select(x =>
            {
                return DateTime.Parse(x.Text).Year;
            }).ToHashSet();
        }
    }
}