def equals(A, B):
    return A == B

def test_givenTwoEqualNumbersAreCompared_Equals_ReturnsTrue():
    print("\nThis test compare two equal numbers and checks if it returns true.")

    A = 5
    B = 5

    print(f"\nA number ---------------------------------------------------")
    print(A)
    print(f"\nB number ---------------------------------------------------")
    print(B)

    result = equals(A, B)
	
    assert result == True

def test_givenTwoNotEqualNumbersAreCompared_Equals_ReturnsFalse():
    print("\nThis test compare two equal numbers and checks if it returns true.")

    A = 5
    B = 10

    print(f"\nA number ---------------------------------------------------")
    print(A)
    print(f"\nB number ---------------------------------------------------")
    print(B)

    result = equals(A, B)
	
    assert result == False
